// **************************************************************************
// AutoRouteGenerator
// **************************************************************************

// GENERATED CODE - DO NOT MODIFY BY HAND

// **************************************************************************
// AutoRouteGenerator
// **************************************************************************
//
// ignore_for_file: type=lint

import 'package:auto_route/auto_route.dart' as _i2;
import 'package:flutter/material.dart' as _i8;

import '../home_page.dart' as _i1;
import '../products/products_page.dart' as _i4;
import '../products/single_product_page.dart' as _i5;
import '../settings/settings_page.dart' as _i3;
import '../users/user_profile_page.dart' as _i7;
import '../users/users_page.dart' as _i6;

class AppRouter extends _i2.RootStackRouter {
  AppRouter([_i8.GlobalKey<_i8.NavigatorState>? navigatorKey])
      : super(navigatorKey);

  @override
  final Map<String, _i2.PageFactory> pagesMap = {
    HomeRoute.name: (routeData) {
      final args =
          routeData.argsAs<HomeRouteArgs>(orElse: () => const HomeRouteArgs());
      return _i2.MaterialPageX<dynamic>(
          routeData: routeData, child: _i1.HomePage(key: args.key));
    },
    ProductsRouter.name: (routeData) {
      return _i2.MaterialPageX<dynamic>(
          routeData: routeData, child: const _i2.EmptyRouterPage());
    },
    UsersRouter.name: (routeData) {
      return _i2.MaterialPageX<dynamic>(
          routeData: routeData, child: const _i2.EmptyRouterPage());
    },
    SettingsRouter.name: (routeData) {
      return _i2.MaterialPageX<dynamic>(
          routeData: routeData, child: const _i3.SettingsPage());
    },
    ProductsRoute.name: (routeData) {
      final args = routeData.argsAs<ProductsRouteArgs>(
          orElse: () => const ProductsRouteArgs());
      return _i2.MaterialPageX<dynamic>(
          routeData: routeData, child: _i4.ProductsPage(key: args.key));
    },
    SingleProductRoute.name: (routeData) {
      final pathParams = routeData.inheritedPathParams;
      final args = routeData.argsAs<SingleProductRouteArgs>(
          orElse: () => SingleProductRouteArgs(
              productId: pathParams.getInt('productId')));
      return _i2.MaterialPageX<dynamic>(
          routeData: routeData,
          child:
              _i5.SingleProductPage(key: args.key, productId: args.productId));
    },
    UsersRoute.name: (routeData) {
      return _i2.MaterialPageX<dynamic>(
          routeData: routeData, child: const _i6.UsersPage());
    },
    UserProfileRoute.name: (routeData) {
      final pathParams = routeData.inheritedPathParams;
      final args = routeData.argsAs<UserProfileRouteArgs>(
          orElse: () =>
              UserProfileRouteArgs(userId: pathParams.getInt('userId')));
      return _i2.MaterialPageX<dynamic>(
          routeData: routeData,
          child: _i7.UserProfilePage(key: args.key, userId: args.userId));
    }
  };

  @override
  List<_i2.RouteConfig> get routes => [
        _i2.RouteConfig(HomeRoute.name, path: '/', children: [
          _i2.RouteConfig(ProductsRouter.name,
              path: 'products',
              parent: HomeRoute.name,
              children: [
                _i2.RouteConfig(ProductsRoute.name,
                    path: '', parent: ProductsRouter.name),
                _i2.RouteConfig(SingleProductRoute.name,
                    path: ':productId', parent: ProductsRouter.name)
              ]),
          _i2.RouteConfig(UsersRouter.name,
              path: 'users',
              parent: HomeRoute.name,
              children: [
                _i2.RouteConfig(UsersRoute.name,
                    path: '', parent: UsersRouter.name),
                _i2.RouteConfig(UserProfileRoute.name,
                    path: ':userId', parent: UsersRouter.name)
              ]),
          _i2.RouteConfig(SettingsRouter.name,
              path: 'settings', parent: HomeRoute.name)
        ])
      ];
}

/// generated route for
/// [_i1.HomePage]
class HomeRoute extends _i2.PageRouteInfo<HomeRouteArgs> {
  HomeRoute({_i8.Key? key, List<_i2.PageRouteInfo>? children})
      : super(HomeRoute.name,
            path: '/',
            args: HomeRouteArgs(key: key),
            initialChildren: children);

  static const String name = 'HomeRoute';
}

class HomeRouteArgs {
  const HomeRouteArgs({this.key});

  final _i8.Key? key;

  @override
  String toString() {
    return 'HomeRouteArgs{key: $key}';
  }
}

/// generated route for
/// [_i2.EmptyRouterPage]
class ProductsRouter extends _i2.PageRouteInfo<void> {
  const ProductsRouter({List<_i2.PageRouteInfo>? children})
      : super(ProductsRouter.name, path: 'products', initialChildren: children);

  static const String name = 'ProductsRouter';
}

/// generated route for
/// [_i2.EmptyRouterPage]
class UsersRouter extends _i2.PageRouteInfo<void> {
  const UsersRouter({List<_i2.PageRouteInfo>? children})
      : super(UsersRouter.name, path: 'users', initialChildren: children);

  static const String name = 'UsersRouter';
}

/// generated route for
/// [_i3.SettingsPage]
class SettingsRouter extends _i2.PageRouteInfo<void> {
  const SettingsRouter() : super(SettingsRouter.name, path: 'settings');

  static const String name = 'SettingsRouter';
}

/// generated route for
/// [_i4.ProductsPage]
class ProductsRoute extends _i2.PageRouteInfo<ProductsRouteArgs> {
  ProductsRoute({_i8.Key? key})
      : super(ProductsRoute.name, path: '', args: ProductsRouteArgs(key: key));

  static const String name = 'ProductsRoute';
}

class ProductsRouteArgs {
  const ProductsRouteArgs({this.key});

  final _i8.Key? key;

  @override
  String toString() {
    return 'ProductsRouteArgs{key: $key}';
  }
}

/// generated route for
/// [_i5.SingleProductPage]
class SingleProductRoute extends _i2.PageRouteInfo<SingleProductRouteArgs> {
  SingleProductRoute({_i8.Key? key, required int productId})
      : super(SingleProductRoute.name,
            path: ':productId',
            args: SingleProductRouteArgs(key: key, productId: productId),
            rawPathParams: {'productId': productId});

  static const String name = 'SingleProductRoute';
}

class SingleProductRouteArgs {
  const SingleProductRouteArgs({this.key, required this.productId});

  final _i8.Key? key;

  final int productId;

  @override
  String toString() {
    return 'SingleProductRouteArgs{key: $key, productId: $productId}';
  }
}

/// generated route for
/// [_i6.UsersPage]
class UsersRoute extends _i2.PageRouteInfo<void> {
  const UsersRoute() : super(UsersRoute.name, path: '');

  static const String name = 'UsersRoute';
}

/// generated route for
/// [_i7.UserProfilePage]
class UserProfileRoute extends _i2.PageRouteInfo<UserProfileRouteArgs> {
  UserProfileRoute({_i8.Key? key, required int userId})
      : super(UserProfileRoute.name,
            path: ':userId',
            args: UserProfileRouteArgs(key: key, userId: userId),
            rawPathParams: {'userId': userId});

  static const String name = 'UserProfileRoute';
}

class UserProfileRouteArgs {
  const UserProfileRouteArgs({this.key, required this.userId});

  final _i8.Key? key;

  final int userId;

  @override
  String toString() {
    return 'UserProfileRouteArgs{key: $key, userId: $userId}';
  }
}
