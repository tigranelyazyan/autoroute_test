import 'package:auto_route/auto_route.dart';
import 'package:autoroute_test/home_page.dart';
import 'package:autoroute_test/products/products_page.dart';
import 'package:autoroute_test/products/single_product_page.dart';
import 'package:autoroute_test/settings/settings_page.dart';
import 'package:autoroute_test/users/user_profile_page.dart';
import 'package:autoroute_test/users/users_page.dart';

@MaterialAutoRouter(
  replaceInRouteName: 'Page,Route',
  routes: [
    AutoRoute(path: '/', page: HomePage, children: [
      AutoRoute(
        path: 'products',
        name: 'ProductsRouter',
        page: EmptyRouterPage,
        children: [
          AutoRoute(
            path: '',
            page: ProductsPage,
          ),
          AutoRoute(
            path: ':productId',
            page: SingleProductPage,
          )
        ],
      ),
      AutoRoute(
        path: 'users',
        name: 'UsersRouter',
        page: EmptyRouterPage,
        children: [
          AutoRoute(
            path: '',
            page: UsersPage,
          ),
          AutoRoute(
            path: ':userId',
            page: UserProfilePage,
          ),
        ],
      ),
      AutoRoute(
        path: 'settings',
        name: 'SettingsRouter',
        page: SettingsPage,
      )
    ]),
  ],
)
class $AppRouter {}
