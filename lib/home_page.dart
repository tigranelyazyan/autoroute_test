import 'package:auto_route/auto_route.dart';
import 'package:flutter/material.dart';
import 'package:autoroute_test/routes/router.gr.dart';
import 'package:salomon_bottom_bar/salomon_bottom_bar.dart';

class HomePage extends StatelessWidget {
  HomePage({Key? key}) : super(key: key);

  final destinations = [
    RouteDestination(
      route: const ProductsRouter(),
        icon: const Icon(
          Icons.post_add,
        ),
        name: 'products'),
    RouteDestination(
        route: const UsersRouter(),

        icon: const Icon(
          Icons.person,
        ),
        name: 'users'),
    RouteDestination(
        route: const SettingsRouter(),
        icon: const Icon(
          Icons.settings,
        ),
        name: 'settings'),
  ];

  @override
  Widget build(BuildContext context) {
    return AutoTabsRouter(
        builder: (context, child, animation) {
          var activeIndex = destinations.indexWhere(
            (d) => context.tabsRouter.isRouteActive(d.route.routeName),
          );
          // there might be no active route until router is mounted
          // so we play safe
          if (activeIndex == -1) {
            activeIndex = 0;
          }
          return Row(
            children: [
              NavigationRail(
                extended: true,
                destinations: destinations
                    .map((item) => NavigationRailDestination(
                          icon: item.icon,
                          label: Text(item.name,style: const TextStyle(color: Colors.black,fontSize: 18),),
                        ))
                    .toList(),
                selectedIndex: activeIndex,
                onDestinationSelected: (index) {
                  // use navigate instead set active index
                  context.navigateTo(destinations[index].route);
                },
              ),
              // child is the rendered route stack
              Expanded(child: child)
            ],
          );
        },
        routes: const [
          ProductsRouter(),
          UsersRouter(),
          SettingsRouter(),
        ]);

    //   AutoTabsScaffold(
    //   appBarBuilder: (_, tabsRouter) => AppBar(
    //     backgroundColor: Colors.indigo,
    //     title: const Text('Autoroute test'),
    //     centerTitle: true,
    //     leading: const AutoBackButton(),
    //   ),
    //   backgroundColor: Colors.indigo,
    //   routes: const [
    //     ProductsRouter(),
    //     UsersRouter(),
    //     SettingsRouter(),
    //   ],
    //   bottomNavigationBuilder: (_, tabsRouter) {
    //     return SalomonBottomBar(
    //       margin: const EdgeInsets.symmetric(
    //         horizontal: 20,
    //         vertical: 40,
    //       ),
    //       currentIndex: tabsRouter.activeIndex,
    //       onTap: tabsRouter.setActiveIndex,
    //       items: [
    //         SalomonBottomBarItem(
    //           selectedColor: Colors.amberAccent,
    //           icon: const Icon(
    //             Icons.post_add,
    //             size: 30,
    //           ),
    //           title: const Text('Products'),
    //         ),
    //         SalomonBottomBarItem(
    //           selectedColor: Colors.blue[200],
    //           icon: const Icon(
    //             Icons.person,
    //             size: 30,
    //           ),
    //           title: const Text('Users'),
    //         ),
    //         SalomonBottomBarItem(
    //           selectedColor: Colors.pinkAccent[100],
    //           icon: const Icon(
    //             Icons.settings,
    //             size: 30,
    //           ),
    //           title: const Text('Settings'),
    //         )
    //       ],
    //     );
    //   },
    // );
  }
}

class RouteDestination {
  final PageRouteInfo route;
  final Icon icon;
  final String name;

  RouteDestination({required this.route ,required this.icon, required this.name});
}
