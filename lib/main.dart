import 'package:flutter/material.dart';
import 'package:autoroute_test/routes/router.gr.dart';
import 'package:get_it/get_it.dart';

// final getIt = GetIt.instance;

void main() {
  WidgetsFlutterBinding.ensureInitialized();

  runApp( MyApp());
}
class MyApp extends StatelessWidget {
  final _appRouter = AppRouter();

   MyApp({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    // final router = getIt<AppRouter>();
    return MaterialApp.router(
      debugShowCheckedModeBanner: false,
      title: 'AutoRoute Test',
      routerDelegate: _appRouter.delegate(),
      routeInformationParser: _appRouter.defaultRouteParser(),
      // key: router.navigatorKey,
    );
  }
}
