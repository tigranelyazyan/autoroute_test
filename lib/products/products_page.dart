import 'package:flutter/material.dart';
import 'package:auto_route/auto_route.dart';
import 'package:autoroute_test/data/app_data.dart';
import 'package:autoroute_test/routes/router.gr.dart';
import 'package:autoroute_test/widgets.dart';

class ProductsPage extends StatelessWidget {
  ProductsPage({Key? key}) : super(key: key);
  final products = Product.products;
  @override
  Widget build(BuildContext context) {
    return Center(
      child: Padding(
        padding: const EdgeInsets.symmetric(
          horizontal: 20,
        ),
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            for (int i = 0; i < products.length; i++)
              ProductTile(
                tileColor: products[i].color,
                productTitle: products[i].title,
                onTileTap: () => context.router.push(
                  SingleProductRoute(
                    productId: products[i].id,
                  ),
                ),
              ),
          ],
        ),
      ),
    );
  }
}
