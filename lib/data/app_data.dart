import 'package:flutter/material.dart';

class Product {
  static final products = [
    Product(Colors.amberAccent, 'Product 1', 1),
    Product(Colors.blue, 'Product 2', 2),
    Product(Colors.pinkAccent, 'Product 3', 3),
  ];
  final Color color;
  final String title;
  final int id;

  Product(this.color, this.title, this.id);
}

class User {
  static final users = [
    User(Colors.amberAccent, 1),
    User(Colors.blue, 2),
    User(Colors.pinkAccent, 3),
  ];
  final Color color;
  final int id;

  User(this.color, this.id);
}
